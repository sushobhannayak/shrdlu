import sys
import subprocess


import string
import jpype
import os
import nltk
from nltk.tree import *
import sys
from random import shuffle
import copy

class ParseObj:
    """Parse object. Contains markers for 
    *type: obj (takes object argument), func (takes lambda expression of objects, like lambda x), funcObj (takes double lambda expn of object and function, like lambda f lambda x); 
    *name: the lexical combination, for example 'red' and 'block' combine to form 'red block';
    *func: lambda expression;
    *takes: the type of argument it accepts -- a list with a type denotation corresponding to each lambda variable. For example, an object of type funcObj might have the list [func, obj] as types, i.e. it first takes a function and then an object. 

A combine() funtion is then defined which outputs the derivation of the combined lambda expression."""

    def __init__(self, name, types, func, takes):
        self.name = name
        self.types = types
        self.func = func
        self.takes = takes

    def __str__(self):
        return "name:"+self.name+" type:"+self.types+" takes:"+string.join(self.takes, ';')

    def combine(self, obj):
        func = self.func(obj.func)
        name = self.name+" "+obj.name
        print name
        takes = self.takes
        del(takes[0])
        types = "func" if takes[0] == 'obj' else "funcObj"
        return ParseObj(name, types, func, takes)
        


""" The following code snippet is the interface to Sanford Parser, from Luke allen. Make sure to change the JAVA_HOME path variables and jar file paths to your system specifications before using."""

'''
@author: Luke Allen
To make this program work, you need to also:
-Install JPype, for your Python version, from http://jpype.sourceforge.net/
-Download the Stanford Parser from http://nlp.stanford.edu/software/lex-parser.shtml
 and copy its .jar files to a folder called "../jar/" relative to this .py file, 
 or else change the line labeled "directory that .jar files are in" below.
'''

os.environ['java_home']='/usr/lib/jvm/java-7-oracle/'
os.environ['JAVA_HOME']='/usr/lib/jvm/java-7-oracle/'
jarpath = os.path.join(os.path.abspath('.'), "./StanParser") #directory that .jar files are in, relative to this .py script.
jpype.startJVM(jpype.getDefaultJVMPath(),"-ea", "-Djava.ext.dirs=%s" % jarpath) #start a JVM
nlp = jpype.JPackage("edu.stanford.nlp") #this is how you import a package (which contains classes). Now we can use the classes inside it, almost as easily as Java.

#code below is a simplified version of the ParserDemo.java included with the Stanford Parser
lp = nlp.parser.lexparser.LexicalizedParser.loadModel() #call a static method of a class like this (see the Stanford Parser's Javadoc for a description of its methods and classes) 
parse = lp.apply(sys.argv[1])
#parse = lp.apply("John pickup red block"); #now we have a Tree class and can do what we want with it
print parse
parse.pennPrint();
tparse = nltk.tree.Tree.parse
tree = tparse(parse.__str__())
print tree

# tree.draw()
# for subtree in tree:
#     print subtree
print "Grammatical Structure:"
tlp = nlp.trees.PennTreebankLanguagePack()
gsf = tlp.grammaticalStructureFactory()
gs = gsf.newGrammaticalStructure(parse)
tdl = gs.typedDependenciesCCprocessed(True)
print tdl

jpype.shutdownJVM() #call at the end of the script

'''Lexicon is a dictionary with mapping from literals to ParseObj objects. '''
"""Each one a ParseObj, with name, types, func, takes"""

Lexicon = {}
funcIS = lambda f: lambda g: lambda x: (f(lambda y: True))(x) * g(x)
Lexicon["is"] = ParseObj("is", 'funcObj', funcIS, ['funcObj', 'func', 'obj'])
funcPICKUP =  lambda f: lambda g: lambda x: lambda y: f(x) * g(y) * PICKUP(x,y)
Lexicon["pickup"] = ParseObj('pickup', 'funcObj', funcPICKUP, ['func', 'func', 'obj', 'obj'])
funcPUT = lambda f: lambda g: lambda h: lambda x: lambda y: lambda z: f(x) * g(y) * h(z) * PUT(x,y,z)
Lexicon["put"] = ParseObj('put', 'funcObj', funcPUT, ['func', 'func', 'func', 'obj', 'obj', 'obj'])
funcBLOCK = lambda x: BLOCK(x)
Lexicon["block"] = ParseObj('block', 'func', funcBLOCK, ['obj'])
funcJOHN = lambda x: JOHN(x)
Lexicon["John"] = ParseObj('John', 'func', funcJOHN, ['obj'])
funcTABLE = lambda x: TABLE(x)
Lexicon["table"] = ParseObj('table', 'func', funcTABLE, ['obj'])
funcRED = lambda f: lambda x: f(x) * RED(x)
Lexicon["red"] = ParseObj('red', 'funcObj', funcRED, ['func', 'obj'])
funcON = lambda f: lambda g: lambda x: lambda y: f(x) * g(y) * ON(y,x)
Lexicon["on"] = ParseObj('on', 'funcObj', funcON, ['func', 'func', 'obj', 'obj'])
funcBEHIND = lambda f: lambda g: lambda x: lambda y: f(x) * g(y) * BEHIND(y,x)
Lexicon["behind"] = ParseObj('on', 'funcObj', funcBEHIND, ['func', 'func', 'obj', 'obj'])
funcTHE = lambda f: lambda x: THE(w,f) * f(x)
Lexicon["the"] = ParseObj('the', 'funcObj', funcTHE, ['func', 'obj'])
funcEVERY = lambda f: lambda g: EVERY(w,f,g)
Lexicon["every"] = ParseObj('every', 'funcObj', funcEVERY, ['func', 'funcObj'])

# print Lexicon['red']
# redBlock =  Lexicon['red'].combine(Lexicon['block'])
# print redBlock
# print Lexicon['the'].combine(redBlock)
# print redBlock.combine(Lexicon['the'])

def combineList(sentence):
    """ Combines a list of predicates. Randomly shuffles the list, and combines in order, taking care of the permissible combinations. If ordering is inconsistent, it goes back to the shuffle, repeating until a permisible combination is found. If permisible combination is not found in 50 odd iterations, returns None. Has the obvious drawback of sometimes leading to bad combinations when multiple combinations are feasible. Possible solutions include: 
1. Introduction of more constraint variables like agent, patient etc. in addition to the used triad (obj, func, funcObj), though that also has its shortcomings like agents/patients being context dependent.
2. Dependency parse and then random combination. """
    # return Lexicon['block']
    print "sentence is: "
    for item in sentence:
        print item
    print "END SENTENCE"
    if len(sentence) <= 1:
        return sentence[0]
    status = False
    temp = []
    iters = 0
    while not status and iters<50:
        iters += 1
        temp = sentence[:]
        print "temp is", temp
        shuffle(temp)
        print "shuffled temp", temp
        while len(temp) > 1:
            if temp[0].takes[0] == temp[1].types:
                newParse = temp[0].combine(temp[1])
                temp[0] = newParse
                del(temp[1])
                status = True if len(temp) == 1 else False
                print "temp is:", temp, "status: ", status
                continue
            elif temp[1].takes[0] == temp[0].types:
                newParse = temp[1].combine(temp[0])
                temp[0] = newParse
                del(temp[1])
                status = True if len(temp) == 1 else False
                print "temp is:", temp, "status: ", status
                continue
            else:
                break
    if iters >= 50:
        print "Incompatible types: The sentence can't be parsed!"
        return None
    print "COMBINED PHRASE: ", temp[0]
    return temp[0]
            
        
def returnTree(tree):
    if not isinstance(tree, Tree):
        return copy.deepcopy(Lexicon[tree])
    else:
        sentence = []
        for subtree in tree:
            print subtree
            sentence.append(returnTree(subtree))
        return combineList(sentence)

# print combineList([copy.deepcopy(Lexicon['block']), copy.deepcopy(Lexicon['the']), copy.deepcopy(Lexicon['red'])])
parsedObj = returnTree(tree)
print parsedObj

#subprocess.call(['./pythonParse.py', 'John pickup the red ball'])

#execfile('pythonParse.py')
##################################################################################################################################
def JOHN(x):
    print "in JOHN, x =", x 
    return 5 if x == 'john' else -5

def BLOCK(x):
    print "in BLOCK, x =", x 
    return 9 if x == 'block' else -9

def RED(x):
    print "in RED, x =", x 
    return 7

def PICKUP(x,y):
    print "in PICK, x,y =", x, y 
    return 3

print parsedObj.func('john')('block')
