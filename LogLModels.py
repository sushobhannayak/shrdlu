import numpy as np
import math
import operator
import copy

class Model:
    """ Model class: defines a model through a list of attributes and corresponding weights"""
    def __init__(self, attrs=[], weights=[]):
        self.attrs = attrs
        self.weights = weights
        
    def __str__(self):
        return self.attrs.__str__() + "\n" + self.weights.__str__()

class LogLModels:
    """Log-likelihood model class. Has models for the lexicons."""
    def __init__(self):
        singletons = ['red', 'blue', 'block', 'ball', 'john', 'table']
        self.singletons = [" "+x for x in singletons]
        # the singletons in parse have a space before them, hence this gymnastics

        self.doubles = ['on', 'behind']

        single_attrs = ['center', 'bound', 'solidity', 'roundness', 'color']
        # center and bound are triplets. So the weight list is of length 9, all initialized to 0's
        single_weights = [0,0,0, 0,0,0, 0, 0, 0]
        self.theta = {} #A dictionary of attribute-parameter(theta) pairs
        for singleton in self.singletons:
            self.theta[singleton] = Model(single_attrs, single_weights)
        
        double_attrs = ['support', 'axes-distance', 'center-distance'] # axes-distance is he distance between x,y and z values
        double_weights = [0,0,0,0,0]
        for double in self.doubles:
            self.theta[double] = Model(double_attrs, double_weights)
        
    """ Definition of log-linear models. Each object is a dictionary with key-value pairs, the log-linear model is built on those keys. x is the name and obj1-2 are arguments for the factor x"""
    def getLogLVal(self, x, obj1, obj2=None):
        attrs = self.theta[x].attrs
        weights = self.theta[x].weights
        indices = []

        if x in self.singletons:
            for attr in attrs:
                if attr=='center' or attr=='bound':
                    indices += obj1[attr] # Because center and bound are triplets
                else:
                    indices += [obj1[attr]]
        else:
            for attr in attrs:
                if attr=='axes-distance':
                    indices += self.getAttrVal(attr, obj1, obj2)
                else:
                    indices += [self.getAttrVal(attr, obj1, obj2)]
        return sum(map(operator.mul, weights, indices))

    def getAttrVal(self, attr, obj1, obj2):
        if attr=='support':
            return 1 if (obj1['center'][2]-obj1['bound'][2])==(obj2['center'][2]+obj2['bound'][2]) else -1
        if attr=='axes-distance':
            return map(operator.sub, obj1['center'], obj2['center'])
        if attr=='center-distance':
            return sum(map(operator.abs, map(operator.sub, obj1['center'], obj2['center'])))

    def updateWeight(self, x, delta):
        # Gradient ascent: so add weights theta += eta*delta
        eta = 0.001
        self.theta[x].weights = map(operator.add, self.theta[x].weights, [eta*i for i in delta])

class MiniLogLModels(LogLModels):
    """Log-likelihood model class. Has models for the lexicons."""
    def __init__(self):
        singletons = ['red', 'blue', 'block', 'ball', 'john', 'table']
        self.singletons = [" "+x for x in singletons]
        # the singletons in parse have a space before them, hence this gymnastics

        self.doubles = ['on', 'behind']

        single_attrs = ['solidity', 'roundness', 'color']
        # center and bound are triplets. So the weight list is of length 9, all initialized to 0's
        single_weights = [0, 0, 0]
        self.theta = {} #A dictionary of attribute-parameter(theta) pairs
        for singleton in self.singletons:
            self.theta[singleton] = Model(single_attrs, single_weights)
        
        double_attrs = ['support'] # axes-distance is he distance between x,y and z values
        double_weights = [0]
        for double in self.doubles:
            self.theta[double] = Model(double_attrs, double_weights)
        
    def getLogLValSupport(self, x, support):
        return self.theta[x].weights[0]*support
