import itertools
import operator
import math

""" Haven't yet taken care of the case when there are two `reds' etc. """
def getGrad(world, parsedObj, logl):
    print world
    print parsedObj

    n = len(world)
    r = len(parsedObj['objects'])
    if r==0 or n==0:
        return logl
    objects = parsedObj['objects']
    parse = parsedObj['parse']

    perms = [x for x in itertools.permutations(range(n), r)]
    print perms
    
    probs = []
    for assign in perms:
        print "assignment is ", assign
        prob = 0
        #create mapping from objects to assignment
        objectMap = {}
        for idx in range(r):
            objectMap[objects[idx]] = assign[idx]
        print "objectMap is ", objectMap
        for item in parse:
            print "parseItem is ", item
            if item['name'] in logl.singletons:
                prob += logl.getLogLVal(item['name'], world[objectMap[item['subj']]])
            elif item['name'] in logl.doubles:
                prob += logl.getLogLVal(item['name'], world[objectMap[item['subj']]], world[objectMap[item['obj1']]])
        probs += [prob]
    print probs #log probs -- normalize in logsumexp
    normalization = math.log(sum([math.exp(x) for x in probs]))
    probs = [x-normalization for x in probs]
    probs = [math.exp(x) for x in probs]
    print "probs", probs

    # Build E(phi) w.r.t p(r|w,s)
    # create initial vals for all items
    ####################################################################
    """ The following is the general implementation for LogLModels"""
    # allVals = {}
    # for item in parse:
    #     if item['name'] in logl.singletons:
    #         allVals[item['name']] = [0,0,0,0,0,0,0,0,0]
    #     if item['name'] in logl.doubles:
    #         allVals[item['name']] = [0,0,0,0,0]
                        

    # for idx in range(len(perms)):
    #     objectMap = {}
    #     for idx in range(r):
    #         objectMap[objects[idx]] = assign[idx]
    #     for item in parse:
    #         if item['name'] in logl.singletons:
    #             obj = world[objectMap[item['subj']]]
    #             # Convert values to list
    #             vals = []
    #             vals += obj['center']
    #             vals += obj['bound']
    #             vals += [obj['solidity'], obj['roundness'], obj['color']]
    #             vals = [probs[idx]*x for x in vals]
    #             allVals[item['name']] = map(operator.add, allVals[item['name']], vals)
    #         elif item['name'] in logl.doubles:
    #             obj1 = world[objectMap[item['subj']]]
    #             obj2 = world[objectMap[item['obj1']]]
    #             # Create value list
    #             vals = [logl.getAttrVal('support', obj1, obj2)]
    #             vals += logl.getAttrVal('axes-distance', obj1, obj2)
    #             vals += [logl.getAttrVal('center-distance', obj1, obj2)]
    #             vals = [x*probs[idx] for x in vals]
    #             allVals[item['name']] = map(operator.add, allVals[item['name']], vals)
    # print allVals
    ###################################################################

    """ Implementation for toy problem MiniLogLModels"""
    allVals = {}
    for item in parse:
        if item['name'] in logl.singletons:
            allVals[item['name']] = [0,0,0]
        if item['name'] in logl.doubles:
            allVals[item['name']] = [0]
                        

    for idx in range(len(perms)):
        objectMap = {}
        for idx in range(r):
            objectMap[objects[idx]] = assign[idx]
        for item in parse:
            if item['name'] in logl.singletons:
                obj = world[objectMap[item['subj']]]
                # Convert values to list
                vals = []
                vals += [obj['solidity'], obj['roundness'], obj['color']]
                vals = [probs[idx]*x for x in vals]
                allVals[item['name']] = map(operator.add, allVals[item['name']], vals)
            elif item['name'] in logl.doubles:
                obj1 = world[objectMap[item['subj']]]
                obj2 = world[objectMap[item['obj1']]]
                # Create value list
                vals = [logl.getAttrVal('support', obj1, obj2)]
                vals = [x*probs[idx] for x in vals]
                allVals[item['name']] = map(operator.add, allVals[item['name']], vals)
    print allVals



    # Finding E(phi) with p(w,r|s) for MiniLogLModels
    """ Notice that in this case, there are only two objects. Each object has three binary features and they have a combined binary feature. So, in total, 6*6*2 possibilities."""

    # First, create a list of the 6 possible objects
    objectList = []
    for i in [1,-1]:
        for j in [1, -1]:
            for k in [1, -1]:
                objectList += [{'solidity':i, 'roundness':j, 'color':k}]

    print objectList
    possibleWorlds = []
    if r==1:
        possibleWorlds = objectList
    else:
        for i in objectList:
            for j in objectList:
                possibleWorlds += [[i,j]]
    #print possibleWorlds

    # Get the world probabilities
    worldProbs = []
    for world in possibleWorlds:
        if r>1:
            for i in [1, -1]:
                prob = 0
                objectMap = {}
                for idx in range(r):
                    objectMap[objects[idx]] = world[idx]
                for item in parse:
                    if item['name'] in logl.singletons:
                        prob += logl.getLogLVal(item['name'], objectMap[item['subj']])
                    elif item['name'] in logl.doubles:
                        prob += logl.getLogLValSupport(item['name'], i)
                worldProbs += [prob]                        
        else:
            prob = 0
            for item in parse:
                if item['name'] in logl.singletons:
                    prob += logl.getLogLVal(item['name'], world)
            worldProbs += [prob]
    normalization = math.log(sum([math.exp(x) for x in worldProbs]))
    worldProbs = [x-normalization for x in worldProbs]
    worldProbs = [math.exp(x) for x in worldProbs]
    print "worldProbs", worldProbs, sum(worldProbs)

    # Now get expectation as before
    newVals = {}
    for item in parse:
        if item['name'] in logl.singletons:
            newVals[item['name']] = [0,0,0]
        if item['name'] in logl.doubles:
            newVals[item['name']] = [0]
            
    probIdx = 0
    if r > 1:
        for world in possibleWorlds:
            for i in [1,-1]:
                objectMap = {}
                for idx in range(r):
                    objectMap[objects[idx]] = world[idx]
                for item in parse:
                    if item['name'] in logl.singletons:
                        obj = objectMap[item['subj']]
                        vals = [obj['solidity'], obj['roundness'], obj['color']]
                        vals = [worldProbs[probIdx]*x for x in vals]
                        newVals[item['name']] = map(operator.add, newVals[item['name']], vals)
                    elif item['name'] in logl.doubles:
                        newVals[item['name']][0] += worldProbs[probIdx]*i
                probIdx += 1
    else:
        for world in possibleWorlds:
            for item in parse:
                if item['name'] in logl.singletons:
                    vals = [world['solidity'], world['roundness'], world['color']]
                    vals = [worldProbs[probIdx]*x for x in vals]
                    newVals[item['name']] = map(operator.add, newVals[item['name']], vals)
            probIdx += 1
                        
    print newVals
                        

    # Update logl weights with these E(phi) 
    for item in allVals:
        logl.updateWeight(item, map(operator.sub, allVals[item], newVals[item]))
    for item in logl.singletons:
        print logl.theta[item]
    for item in logl.doubles:
        print logl.theta[item]

        
    return logl
