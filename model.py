import math

class World:
    """The world class. All worlds are instances of this class. In its basic form, it has a list of entities, each of which is an instance of Object class or its derivatives. The information about location,direction etc. are part of the object feature. This might be provided through an external interface, say a church program output.  """

    Entities = []


class Object:
    """The object class. All physical objects are a derivative of this class. This class may include a feature list, Features, which contains items from the feature class. It might also provide methods for outputting distributions based on the world, which might be a log-linear model in which case, the weight vector is used."""

    Features = []
    Weights = []

    """ The ditribution method, which gives a probabilistic distribution as output.  The distribution can also include a delta function which limits the kinds of arguments it can process. It's basically a log-linear model on the Features, the weights of which might either be decided through training or are hard coded."""
    def distribution(self):
        pass

class Feature:
    """ The feature class. Denotes different feature types. All features, like position, direction, motion etc. are derivatives of this class. Provides methods for interaction between features or their modification. The subclasses implement methods specific to their type, like distance, composition etc. """
    pass


class Action:
    """The action class. All verb predicates are derivatives of this class. It provides methods for outputting a probability distribution given multiple arguments and a world. The distribution can also include a delta function which limits the kinds of arguments it can process."""
    def distribution(self, objectList):
        pass

    """ Another option is to describe all the predicates as methods of this class. """

class Parser:
    """ Takes as input an English sentence and gives out the clausal form logic of the sentence. The clausal form is then further used in generating distributions. Contains lexicon, with mappings from word to meaning (lambda calculus predicates). """

    Lexicon = {}

    def toClause(self, sentence="", world=[]):
        clause = []
        return clause


class Location(Feature):
    """ Location feature, consists of a list of coordinates. Might provide methods inherent to locations like distance calculation etc."""
    location = []

    def __init__(loc):
        self.location = loc

    def dist(self, loc):
        pass
    
    

class InferenceMachine:
    """ The main class. Contains methods for inference. Coordinates between different classes and given a sentence, generates parses and distributions."""

    #get sentence
    pass

    #get world
    world = []

    #generate clausal form: a list of lists
    clauses = Parser.toClause(sentence, world)

    

    for clause in clauses:
        for item in clause:
            """ Determine probability distribution by calling appropriate methods of appropriate classes. In the inner loop probabilities are added, while they are multiplied in the outer loop. """
            pass



    
    pass
