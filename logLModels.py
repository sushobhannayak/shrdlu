import numpy as np
import math
import operator
import copy
# ##################################################################################################################################
# def JOHN(x):
#     print "in JOHN, x =", x 
#     return 5 if x == 'john' else -5

# def BLOCK(x):
#     print "in BLOCK, x =", x 
#     return 9 if x == 'block' else -9

# def RED(x):
#     print "in RED, x =", x 
#     return 7

# def PICKUP(x,y):
#     print "in PICK, x,y =", x, y 
#     return 3


class logLModels:
    """ Definition of log-linear models. Each object is a dictionary with key-value pairs, the log-linear model is built on those keys."""
    def john(self, x):
        attrs = []
        weights = []
        indices = []
        for attr in attrs:
            if attr in x:
                indices.append(x[attr])
            else:
                indices.append(0)
        return math.exp(sum(map(operator.mul, weights, indices)))/math.exp(sum(map(operator.mul, weights, np.ones(len(weights)))))

    def block(self, x):
        attrs = ['block']
        weights = [1]
        indices = []
        for attr in attrs:
            if attr in x:
                indices.append(x[attr])
            else:
                indices.append(0)
        return math.exp(sum(map(operator.mul, weights, indices)))/math.exp(sum(map(operator.mul, weights, np.ones(len(weights)))))

    def red(self, x):
        attrs = ['red']
        weights = [1]
        indices = []
        for attr in attrs:
            if attr in x:
                indices.append(x[attr])
            else:
                indices.append(0)
        return math.exp(sum(map(operator.mul, weights, indices)))/math.exp(sum(map(operator.mul, weights, np.ones(len(weights)))))

    def table(self, x):
        attrs = ['table']
        weights = [1]
        indices = []
        for attr in attrs:
            if attr in x:
                indices.append(x[attr])
            else:
                indices.append(0)
        return math.exp(sum(map(operator.mul, weights, indices)))/math.exp(sum(map(operator.mul, weights, np.ones(len(weights)))))

    def on(self, x,y):
        #suppose x and y defined by their bounding boxes, which are denoted with a lower and upper opposite corners, like (0,0,0) and (5,5,5)
        if x['lower'][2] == y['upper'][2]:
            return 1
        return 0

    def iss(self, a):
        return 1
    
    def the(self, world, func):
        #print world
        #print func
        p_tot = 0
        if func == 'block':
            for obj in world:
                p = self.block(world[obj])
                #print 'p=', p, 'obj=', obj
                rest = copy.deepcopy(world)
                del(rest[obj])
                for new_obj in rest:
                    p *= (1-self.block(world[new_obj]))
                    #print 'p=', p, 'new_obj=', new_obj
                p_tot += p
        if func == 'table':
            for obj in world:
                p = self.table(world[obj])
                #print 'p=', p, 'obj=', obj
                rest = copy.deepcopy(world)
                del(rest[obj])
                for new_obj in rest:
                    p *= (1-self.table(world[new_obj]))
                    #print 'p=', p, 'new_obj=', new_obj
                p_tot += p
        return p_tot

    def every(self, world, func, parsedObj):
        
	""" 'Every' is taken care of in the inference function of the calling script -- so just return 1"""
        # p_tot = 1
        # f = self.table if func == 'table' else self.block
        # for obj in world:
        #     p = 1
        #     if len(parsedObj['objects']) == 1:
        #         p *= inference(world, [world[obj]], parsedObj)
        #     if len(parsedObj['objects']) == 2:
        #         for obj2 in world:
                    

        #     p = 1
        return 1
