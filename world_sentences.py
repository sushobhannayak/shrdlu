""" Define worlds and sentences describing the world. A world description is a list of objects, each of which is a dictionary of feature-value pairs. 'worlds' variable is a list of such worlds. The sentences list is a list of lists, where each component list is a list of sentences describing that world. Define the function getWorldSentPair in WorldSents class which gives out a list of world-sentence pairings. """
red = 1
blue = -1
worlds = \
[\
[\
{	"center": [40, 60, 50],\
	"bound" : [40, 60, 50],\
	"solidity" : -1,\
	"roundness" : -1,\
	"color" : red	\
},\
{	"center": [60, 40, 110],\
	"bound" : [10, 10, 10],\
	"solidity" : 1,\
	"roundness" : -1,\
	"color" : red\
},\
{	"center": [60, 40, 127.5],\
	"bound" : [5, 5, 7.5],\
	"solidity" : 1,\
	"roundness" : -1,\
	"color" : 	blue\
},\
{	"center": [45, 40, 105],\
	"bound" : [5, 5, 5],\
	"solidity" : 1,\
	"roundness" : 1,\
	"color" : blue\
}\
],\
[\
{	"center": [50, 50, 50],\
	"bound" : [50, 50, 50],\
	"solidity" : -1,\
	"roundness" : -1,\
	"color" : red	\
},\
{	"center": [50, 50, 110],\
	"bound" : [10, 10, 10],\
	"solidity" : 1,\
	"roundness" : 1,\
	"color" : blue\
}\
],\
[\
{	"center": [50, 50, 7.5],\
	"bound" : [7.5, 10, 7.5],\
	"solidity" : 1,\
	"roundness" : -1,\
	"color" : blue\
},\
{	"center": [50, 50, 30],\
	"bound" : [15, 15, 15],\
	"solidity" : 1,\
	"roundness" : 1,\
	"color" : blue\
}\
],\
[\
{	"center": [15, 15, 7.5],\
	"bound" : [15, 15, 7.5],\
	"solidity" : 1,\
	"roundness" : -1,\
	"color" : blue\
},\
{	"center": [5, 5, 20],\
	"bound" : [5, 5, 5],\
	"solidity" : 1,\
	"roundness" : -1,\
	"color" : red\
},\
{	"center": [80, 15, 15],\
	"bound" : [15, 15, 15],\
	"solidity" : 1,\
	"roundness" : 1,\
	"color" : red\
},\
{	"center": [80, 80, 20],\
	"bound" : [15, 15, 20],\
	"solidity" : 1,\
	"roundness" : -1,\
	"color" : red\
}\
],\
[\
{	"center": [50, 50, 50],\
	"bound" : [50, 50, 50],\
	"solidity" : 1,\
	"roundness" : -1,\
	"color" : red\
},\
{	"center": [50, 50, 60],\
	"bound" : [10, 10, 10],\
	"solidity" : 1,\
	"roundness" : -1,\
	"color" : blue\
},\
{	"center": [50, 50, 125],\
	"bound" : [5, 5, 5],\
	"solidity" : 1,\
	"roundness" : -1,\
	"color" : blue\
},\
],\
[\
{	"center": [50, 50, 7.5],\
	"bound" : [7.5, 10, 7.5],\
	"solidity" : 1,\
	"roundness" : -1,\
	"color" : blue\
},\
{	"center": [15, 50, 15],\
	"bound" : [15, 15, 15],\
	"solidity" : 1,\
	"roundness" : 1,\
	"color" : blue\
}\
]\
]

sents = [["ball is on table", "blue ball is on table", "red block is on table", "ball is behind red block", "blue ball is behind red block", "ball is behind block", "blue block is on red block", "blue block is on table"], ["blue ball", "red table", "blue ball is on table", "blue ball is on red table", "ball is on red table"], ["blue block", "blue ball is on block", "blue ball is on blue block", "blue ball", "ball is on block"], ["red ball", "blue block", "red block", "blue block is behind ball", "blue block is behind red ball", "block is on blue block", "red block is on blue block", "block is behind ball"], ["red ball is on blue block", "red ball is on blue block", "ball is on blue block", "block is on red block", "ball is on block"], ["blue block", "ball", "blue ball", "ball is behind block", "blue ball is behind blue block", "ball is behind blue block", "blue ball is behind block"]]

class WorldSents():

	def __init__(self, worlds, sents):
		self.worlds = worlds
		self.sents = sents

	def getWorldSentPairs(self):
		worldSentPair = []
		for x in range(len(self.worlds)):
			for sent in self.sents[x]:
				worldSentPair.append((self.worlds[x], sent))
		return worldSentPair

