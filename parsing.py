import nltk
from nltk.tree import *


w = [1,2,3,4,5,6,7]
def RED(x):
    if x in [1,7,6]:
        return True
    else:
        return False
    pass

def JOHN(x):
    if x in [3, 4, 5]:
        return True
    else:
        return False
    pass

def BLOCK(x):
    if x in [1, 6, 7]:
        return True
    else:
        return False
    pass

def TABLE(x):
    pass

def ON(y,x):
    pass

def BEHIND(y,x):
    pass

def PICKUP(x,y):
    if (x,y) in [(1,3), (6,5), (7,3)]:
        return True
    else:
        return False

    pass

def PUT(x,y,z):
    pass

def EVERY(w,f,g):
	x = True
	for item in w:
		x = x and (not f(item) or g(lambda y:True)(item))
	return x

def THE(w,f):
	x = False
	for o in w:
		y = True
		for m in w:
			if m == o:
				y = y and f(m)
			else:
				y = y and not f(m)
			
		x = x or y
	return x

Lexicon = {}


Lexicon["is"] = lambda f: lambda g: lambda x: (f(lambda y: True))(x) and g(x)
Lexicon["pickup"] = lambda f: lambda g: lambda x: lambda y: f(x) and g(y) and PICKUP(x,y)
Lexicon["put"] = lambda f: lambda g: lambda h: lambda x: lambda y: lambda z: f(x) and g(y) and h(z) and PUT(x,y,z)
Lexicon["block"] = lambda x: BLOCK(x)
Lexicon["John"] = lambda x: JOHN(x)
Lexicon["table"] = lambda x: TABLE(x)
Lexicon["red"] = lambda f: lambda x: f(x) and RED(x)
Lexicon["on"] = lambda f: lambda g: lambda x: lambda y: f(x) and g(y) and ON(y,x)
Lexicon["behind"] = lambda f: lambda g: lambda x: lambda y: f(x) and g(y) and BEHIND(y,x)
Lexicon["the"] = lambda f: lambda x: THE(w,f) and f(x)                                                        #w = world
Lexicon["every"] = lambda f: lambda g: EVERY(w,f,g)

redBlock = Lexicon["red"](Lexicon["block"])
pickupBlockJohn = (Lexicon['pickup'](Lexicon["block"]))(Lexicon["John"])

tparse = nltk.tree.Tree.parse
tree = tparse("(ROOT (FRAG (NP (DT the) (JJ red) (NN block))))")
print tree
def returnTree(tree):
    if not isinstance(tree, Tree):
	#print tree
        return Lexicon[tree]
    else:
        f = lambda x: x
        for subtree in tree:
		print subtree	
		f = f(returnTree(subtree))
	return f
print returnTree(tree)
		
