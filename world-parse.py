wspair = [[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [40, 60, 50], 'bound': [40, 60, 50]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [60, 40, 110], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [60, 40, 127.5], 'bound': [5, 5, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [45, 40, 105], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'1', 'name': u' ball'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'4', 'subj': u'1', 'name': u'on'}, {'subj': u'4', 'name': u' table'}], 'objects': [u'1', u'4']}],[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [40, 60, 50], 'bound': [40, 60, 50]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [60, 40, 110], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [60, 40, 127.5], 'bound': [5, 5, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [45, 40, 105], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' ball'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'5', 'subj': u'2', 'name': u'on'}, {'subj': u'5', 'name': u' table'}], 'objects': [u'2', u'5']}],[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [40, 60, 50], 'bound': [40, 60, 50]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [60, 40, 110], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [60, 40, 127.5], 'bound': [5, 5, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [45, 40, 105], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'2', 'name': u' red'}, {'subj': u'2', 'name': u' block'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'5', 'subj': u'2', 'name': u'on'}, {'subj': u'5', 'name': u' table'}], 'objects': [u'2', u'5']}],[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [40, 60, 50], 'bound': [40, 60, 50]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [60, 40, 110], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [60, 40, 127.5], 'bound': [5, 5, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [45, 40, 105], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'1', 'name': u' ball'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'5', 'subj': u'1', 'name': u'behind'}, {'subj': u'5', 'name': u' red'}, {'subj': u'5', 'name': u' block'}], 'objects': [u'1', u'5']}],[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [40, 60, 50], 'bound': [40, 60, 50]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [60, 40, 110], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [60, 40, 127.5], 'bound': [5, 5, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [45, 40, 105], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' ball'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'6', 'subj': u'2', 'name': u'behind'}, {'subj': u'6', 'name': u' red'}, {'subj': u'6', 'name': u' block'}], 'objects': [u'2', u'6']}],[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [40, 60, 50], 'bound': [40, 60, 50]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [60, 40, 110], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [60, 40, 127.5], 'bound': [5, 5, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [45, 40, 105], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'1', 'name': u' ball'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'4', 'subj': u'1', 'name': u'behind'}, {'subj': u'4', 'name': u' block'}], 'objects': [u'1', u'4']}],[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [40, 60, 50], 'bound': [40, 60, 50]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [60, 40, 110], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [60, 40, 127.5], 'bound': [5, 5, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [45, 40, 105], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' block'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'6', 'subj': u'2', 'name': u'on'}, {'subj': u'6', 'name': u' red'}, {'subj': u'6', 'name': u' block'}], 'objects': [u'2', u'6']}],[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [40, 60, 50], 'bound': [40, 60, 50]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [60, 40, 110], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [60, 40, 127.5], 'bound': [5, 5, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [45, 40, 105], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' block'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'5', 'subj': u'2', 'name': u'on'}, {'subj': u'5', 'name': u' table'}], 'objects': [u'2', u'5']}],[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [50, 50, 50], 'bound': [50, 50, 50]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [50, 50, 110], 'bound': [10, 10, 10]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u'ball'}], 'objects': [u'2']}],[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [50, 50, 50], 'bound': [50, 50, 50]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [50, 50, 110], 'bound': [10, 10, 10]}],{'parse': [{'subj': u'2', 'name': u' red'}, {'subj': u'2', 'name': u'table'}], 'objects': [u'2']}],[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [50, 50, 50], 'bound': [50, 50, 50]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [50, 50, 110], 'bound': [10, 10, 10]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' ball'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'5', 'subj': u'2', 'name': u'on'}, {'subj': u'5', 'name': u' table'}], 'objects': [u'2', u'5']}],[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [50, 50, 50], 'bound': [50, 50, 50]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [50, 50, 110], 'bound': [10, 10, 10]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' ball'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'6', 'subj': u'2', 'name': u'on'}, {'subj': u'6', 'name': u' red'}, {'subj': u'6', 'name': u' table'}], 'objects': [u'2', u'6']}],[[{'roundness': -1, 'color': 1, 'solidity': -1, 'center': [50, 50, 50], 'bound': [50, 50, 50]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [50, 50, 110], 'bound': [10, 10, 10]}],{'parse': [{'subj': u'1', 'name': u' ball'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'5', 'subj': u'1', 'name': u'on'}, {'subj': u'5', 'name': u' red'}, {'subj': u'5', 'name': u' table'}], 'objects': [u'1', u'5']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 7.5], 'bound': [7.5, 10, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [50, 50, 30], 'bound': [15, 15, 15]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u'block'}], 'objects': [u'2']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 7.5], 'bound': [7.5, 10, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [50, 50, 30], 'bound': [15, 15, 15]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' ball'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'5', 'subj': u'2', 'name': u'on'}, {'subj': u'5', 'name': u' block'}], 'objects': [u'2', u'5']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 7.5], 'bound': [7.5, 10, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [50, 50, 30], 'bound': [15, 15, 15]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' ball'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'6', 'subj': u'2', 'name': u'on'}, {'subj': u'6', 'name': u' blue'}, {'subj': u'6', 'name': u' block'}], 'objects': [u'2', u'6']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 7.5], 'bound': [7.5, 10, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [50, 50, 30], 'bound': [15, 15, 15]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u'ball'}], 'objects': [u'2']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 7.5], 'bound': [7.5, 10, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [50, 50, 30], 'bound': [15, 15, 15]}],{'parse': [{'subj': u'1', 'name': u' ball'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'4', 'subj': u'1', 'name': u'on'}, {'subj': u'4', 'name': u' block'}], 'objects': [u'1', u'4']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [15, 15, 7.5], 'bound': [15, 15, 7.5]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [5, 5, 20], 'bound': [5, 5, 5]}, {'roundness': 1, 'color': 1, 'solidity': 1, 'center': [80, 15, 15], 'bound': [15, 15, 15]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [80, 80, 20], 'bound': [15, 15, 20]}],{'parse': [{'subj': u'2', 'name': u' red'}, {'subj': u'2', 'name': u'ball'}], 'objects': [u'2']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [15, 15, 7.5], 'bound': [15, 15, 7.5]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [5, 5, 20], 'bound': [5, 5, 5]}, {'roundness': 1, 'color': 1, 'solidity': 1, 'center': [80, 15, 15], 'bound': [15, 15, 15]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [80, 80, 20], 'bound': [15, 15, 20]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u'block'}], 'objects': [u'2']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [15, 15, 7.5], 'bound': [15, 15, 7.5]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [5, 5, 20], 'bound': [5, 5, 5]}, {'roundness': 1, 'color': 1, 'solidity': 1, 'center': [80, 15, 15], 'bound': [15, 15, 15]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [80, 80, 20], 'bound': [15, 15, 20]}],{'parse': [{'subj': u'2', 'name': u' red'}, {'subj': u'2', 'name': u'block'}], 'objects': [u'2']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [15, 15, 7.5], 'bound': [15, 15, 7.5]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [5, 5, 20], 'bound': [5, 5, 5]}, {'roundness': 1, 'color': 1, 'solidity': 1, 'center': [80, 15, 15], 'bound': [15, 15, 15]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [80, 80, 20], 'bound': [15, 15, 20]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' block'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'5', 'subj': u'2', 'name': u'behind'}, {'subj': u'5', 'name': u' ball'}], 'objects': [u'2', u'5']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [15, 15, 7.5], 'bound': [15, 15, 7.5]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [5, 5, 20], 'bound': [5, 5, 5]}, {'roundness': 1, 'color': 1, 'solidity': 1, 'center': [80, 15, 15], 'bound': [15, 15, 15]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [80, 80, 20], 'bound': [15, 15, 20]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' block'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'6', 'subj': u'2', 'name': u'behind'}, {'subj': u'6', 'name': u' red'}, {'subj': u'6', 'name': u' ball'}], 'objects': [u'2', u'6']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [15, 15, 7.5], 'bound': [15, 15, 7.5]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [5, 5, 20], 'bound': [5, 5, 5]}, {'roundness': 1, 'color': 1, 'solidity': 1, 'center': [80, 15, 15], 'bound': [15, 15, 15]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [80, 80, 20], 'bound': [15, 15, 20]}],{'parse': [{'subj': u'1', 'name': u' block'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'5', 'subj': u'1', 'name': u'on'}, {'subj': u'5', 'name': u' blue'}, {'subj': u'5', 'name': u' block'}], 'objects': [u'1', u'5']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [15, 15, 7.5], 'bound': [15, 15, 7.5]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [5, 5, 20], 'bound': [5, 5, 5]}, {'roundness': 1, 'color': 1, 'solidity': 1, 'center': [80, 15, 15], 'bound': [15, 15, 15]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [80, 80, 20], 'bound': [15, 15, 20]}],{'parse': [{'subj': u'2', 'name': u' red'}, {'subj': u'2', 'name': u' block'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'6', 'subj': u'2', 'name': u'on'}, {'subj': u'6', 'name': u' blue'}, {'subj': u'6', 'name': u' block'}], 'objects': [u'2', u'6']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [15, 15, 7.5], 'bound': [15, 15, 7.5]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [5, 5, 20], 'bound': [5, 5, 5]}, {'roundness': 1, 'color': 1, 'solidity': 1, 'center': [80, 15, 15], 'bound': [15, 15, 15]}, {'roundness': -1, 'color': 1, 'solidity': 1, 'center': [80, 80, 20], 'bound': [15, 15, 20]}],{'parse': [{'subj': u'1', 'name': u' block'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'4', 'subj': u'1', 'name': u'behind'}, {'subj': u'4', 'name': u' ball'}], 'objects': [u'1', u'4']}],[[{'roundness': -1, 'color': 1, 'solidity': 1, 'center': [50, 50, 50], 'bound': [50, 50, 50]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 60], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 125], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'2', 'name': u' red'}, {'subj': u'2', 'name': u' ball'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'6', 'subj': u'2', 'name': u'on'}, {'subj': u'6', 'name': u' blue'}, {'subj': u'6', 'name': u' block'}], 'objects': [u'2', u'6']}],[[{'roundness': -1, 'color': 1, 'solidity': 1, 'center': [50, 50, 50], 'bound': [50, 50, 50]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 60], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 125], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'2', 'name': u' red'}, {'subj': u'2', 'name': u' ball'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'6', 'subj': u'2', 'name': u'on'}, {'subj': u'6', 'name': u' blue'}, {'subj': u'6', 'name': u' block'}], 'objects': [u'2', u'6']}],[[{'roundness': -1, 'color': 1, 'solidity': 1, 'center': [50, 50, 50], 'bound': [50, 50, 50]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 60], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 125], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'1', 'name': u' ball'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'5', 'subj': u'1', 'name': u'on'}, {'subj': u'5', 'name': u' blue'}, {'subj': u'5', 'name': u' block'}], 'objects': [u'1', u'5']}],[[{'roundness': -1, 'color': 1, 'solidity': 1, 'center': [50, 50, 50], 'bound': [50, 50, 50]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 60], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 125], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'1', 'name': u' block'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'5', 'subj': u'1', 'name': u'on'}, {'subj': u'5', 'name': u' red'}, {'subj': u'5', 'name': u' block'}], 'objects': [u'1', u'5']}],[[{'roundness': -1, 'color': 1, 'solidity': 1, 'center': [50, 50, 50], 'bound': [50, 50, 50]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 60], 'bound': [10, 10, 10]}, {'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 125], 'bound': [5, 5, 5]}],{'parse': [{'subj': u'1', 'name': u' ball'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'4', 'subj': u'1', 'name': u'on'}, {'subj': u'4', 'name': u' block'}], 'objects': [u'1', u'4']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 7.5], 'bound': [7.5, 10, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [15, 50, 15], 'bound': [15, 15, 15]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u'block'}], 'objects': [u'2']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 7.5], 'bound': [7.5, 10, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [15, 50, 15], 'bound': [15, 15, 15]}],{'parse': [{}], 'objects': []}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 7.5], 'bound': [7.5, 10, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [15, 50, 15], 'bound': [15, 15, 15]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u'ball'}], 'objects': [u'2']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 7.5], 'bound': [7.5, 10, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [15, 50, 15], 'bound': [15, 15, 15]}],{'parse': [{'subj': u'1', 'name': u' ball'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'4', 'subj': u'1', 'name': u'behind'}, {'subj': u'4', 'name': u' block'}], 'objects': [u'1', u'4']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 7.5], 'bound': [7.5, 10, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [15, 50, 15], 'bound': [15, 15, 15]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' ball'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'6', 'subj': u'2', 'name': u'behind'}, {'subj': u'6', 'name': u' blue'}, {'subj': u'6', 'name': u' block'}], 'objects': [u'2', u'6']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 7.5], 'bound': [7.5, 10, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [15, 50, 15], 'bound': [15, 15, 15]}],{'parse': [{'subj': u'1', 'name': u' ball'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'5', 'subj': u'1', 'name': u'behind'}, {'subj': u'5', 'name': u' blue'}, {'subj': u'5', 'name': u' block'}], 'objects': [u'1', u'5']}],[[{'roundness': -1, 'color': -1, 'solidity': 1, 'center': [50, 50, 7.5], 'bound': [7.5, 10, 7.5]}, {'roundness': 1, 'color': -1, 'solidity': 1, 'center': [15, 50, 15], 'bound': [15, 15, 15]}],{'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' ball'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'5', 'subj': u'2', 'name': u'behind'}, {'subj': u'5', 'name': u' block'}], 'objects': [u'2', u'5']}],]