import world_sentences
from world_sentences import *
import LogLModels
from LogLModels import *
import sys
import subprocess

ws = WorldSents(worlds, sents)
wspair = ws.getWorldSentPairs()

logl = MiniLogLModels()
# print logl.getLogLVal('red', wspair[1][0][1])
# print logl.getLogLVal('on', wspair[1][0][1], wspair[1][0][2])

import SGD
import DependencyParser
from DependencyParser import *

#######################################################
# # Create world-parse pairs : one time only to get script
# f = open('world-parse.py', 'w')
# f.write("wspair = [")
# f.close()
# for pair in wspair:
#     f = open('world-parse.py', 'a')
#     f.write("[")
#     f.write(pair[0].__str__())
#     f.write(",")
#     f.close()
#     subprocess.call([sys.executable, 'dependencyParse.py', pair[1]])
#     f = open('world-parse.py', 'a')
#     f.write("],")
#     f.close()
# f = open('world-parse.py', 'a')
# f.write("]")
# f.close()
#########################################################
execfile('world-parse.py')    
print wspair

# depParser = DependencyParser(sents[0][0])
# parsedObj = depParser.returnParse()
# logl = SGD.getGrad(worlds[0], parsedObj, logl)
# parsedObj = {'parse': [{'subj': u'1', 'name': u' ball'}, {'subj': u'1', 'name': u'is'}, {'obj1': u'4', 'subj': u'1', 'name': u'on'}, {'subj': u'4', 'name': u' table'}], 'objects': [u'1', u'4']}
# logl = SGD.getGrad(worlds[0], parsedObj, logl)
# parsedObj = {'parse': [{'subj': u'2', 'name': u' blue'}, {'subj': u'2', 'name': u' ball'}, {'subj': u'2', 'name': u'is'}, {'obj1': u'5', 'subj': u'2', 'name': u'on'}, {'subj': u'5', 'name': u' table'}], 'objects': [u'2', u'5']}
# logl = SGD.getGrad(worlds[0], parsedObj, logl)

from random import shuffle
maxIter = 1000
for _ in range(maxIter):
    idxs = range(len(wspair))
    shuffle(idxs)
    for idx in idxs:
        logl = SGD.getGrad(wspair[idx][0], wspair[idx][1], logl)
