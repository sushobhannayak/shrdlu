\documentclass{article}
\usepackage{amsfonts}
\usepackage{amsmath}
\begin{document}
\section{Introduction}
Understanding human language is a challenging problem that has been
studied by many researchers in diverse fields such as linguistics, artificial
intelligence, and psychology. A prominent school of thought in language understanding and production assumes a grounded view of language, in which lexical semantics is interpreted as a mapping from the lexicon to the physical/perceptual world. -- MAY BE CITE SOME PREV WORK BY SISKIND, REGIER, ROY, MOONEY AND SET UP MOTIVATION FOR A PROBABILISTIC MODEL -- While many of them attempt to specify mappings from single functional words to the world domain, without incorporating context or without handling full sentences, others(Siskind) take an essentially deterministic approach in disambiguating word reference mappings in cross-situational learning. We attempt to bridge the gap by creating a model that can learn language and do inference over worlds in a general setting, and which could incorporate information about sentence composition, context and possibly in the future, pragmatics. We propose a principled probabilistic approach to natural language comprehension and production and show how
it can be used to understand statements, follow imperative commands,
and answer questions. In what follows, we first describe the model for understanding sentences and then discuss a limited implemented version that achieves this goal. 

\section{Understanding Statements}
From a grounded language point of view, understanding language is essentially creating a mapping from the linguistic domain to the perceptual domain. However, at any time, we mostly have a partially observed world -- so communication is essentially the interaction between the language and world model, that enrich each other by more, possibly unobserved, information, which can be viewed as a distribution over possible worlds. To be precise, the agent has a prior distribution over possible worlds, $p(W)$. However, the `world' is not necessarily fully observable: For example, in a blocks-world domain, a world
might consist of a table with a particular location and geometry, along with a
set of blocks that have locations, geometries, colors, shapes, and (unobservable)
categories. In order for an agent to understanding human language, it must incorporate
information from language into this uncertain model of the world, thereby imparting certain aspects to it and infereing information about hitherto unobserved parts of it and use this information
to influence future actions and decisions.

Specifically, consider the case when it receives as input a sentence $\Lambda$ with a meaning representation $M_\Lambda$
which is a stochastic predicate on worlds: a function from worlds to [True, False ],
which indicates whether $\Lambda$ is true in each world. Using this predicate the agent
may construct a posterior distribution, $p(W |M_\Lambda (W )=T )$, over worlds by conditioning on (i.e., ``assuming'') the predicate’s truth. For example, a sentence
might be ``The triangle is near the square.'' After receiving this sentence, the
posterior over worlds should be peaked near worlds with a triangular block that
is physically close to a square block. We can compute this update using Bayes’
rule:
\[p(W |M_\Lambda (W )=T ) \propto p(M_\Lambda (W )=T |W ) \times p(W )\]
Without further simplification, however, this will lead to a combinatorial explosion and become intractable. Fortunately, we can utilize the compositional structure of the input sentence to factor the above equation so that the meaning representation $M_\Lambda$ may be broken down according to the
hierarchical structure of language. We further describe this process which takes as input a parse tree for a sentence, then
constructs a meaning representation as a {\em fold} operation that recursively processes each node in the tree in order to build up a return value. To specify a
particular semantics, we need to specify first the lexical entries for leaves in the
tree, and second the composition operator that combines meanings together at
non-leaf nodes.

\section{Compositional and Lexical Semantics}
A sentence is composed of words, and we treat words as simple experts for reporting whether entities
have particular properties and build sentence meaning via a ``product of experts'' that is influenced by their composition. We assume that each lexical entry, $\lambda$, consists of a stochastic predicate,
$M_\lambda (W ; e, ...)$, that takes as input the world and one or more entities, and returns
a probability -- the probability that the relevant property is true for that (set
of) objects in that world. We treat the input entities, $e$, \ldots, as values of a set of
random variables which are added to the state space of the prior. We will refer
to these variably as ``entity'', ``linguistic'', or ``reference'' variables because they
determine the entities to which particular lexical items refer. We implement these predicates as simple log-linear models over the feature space of the input entity. The feature space for an input can be diverse depending upon the type of the input : For instance, if the entity is an object it
may have associated with it features such as location and pose (while actions,
locations, etc would have different features). The learning part of the model would then determine the weights of the log-linear model given instances of the world and input. 

Assuming that we have learned the log-linear predicates, the meanings of words compose together via simple multiplication of the probabilities returned by each predicate; the hierarchical structure is used to tie together reference variables. For example, `red block on table' is converted to a product of predicates of the form \{ red($e_1$), block($e_1$), table($e_2$), on($e_1$, $e_2$))\}. Notice that along with the hierarchical structure of a parse tree, we also need dependency information to create a proper assignment of input variables to different predicates, so that `red' takes the same argument as `block' and not that of `table'. In particular, we introduce a
delta function at each internal node of the parse tree to enforce equality of
reference variables in the constituents. Overall the meaning of a sentence is
thus (schematically, without representing the specific predicate and hierarchical
structure):
\[p(M_\lambda (W ) = T |W ) \propto \sum_e \prod_{\lambda\in\Lambda}p(M_\lambda (W ; e, \ldots) = T |W ) \prod_{t\in\tau}\delta_{t_{e1} = t_{e2}}\]
It might be noted that we marginalize out the
linguistic variables to represent a pure predicate on worlds, and, the product of
experts does not result in a properly normalized probability (though this rarely
presents an obstacle to most tasks). This model is nice because each factor has the same structure, a log-linear
model, modulo additional grounding arguments. Factors are learned at training time, and at inference time can be simply read in from a dictionary and
multiplied together.
It is also problematic because it may not match traditional approaches to seman-
tics, and because the denominator is not be ignorable in general.
This model is better illustrated through examples with semantic representations of words.

\section{Semantic Representation}
We define $\Lambda$ to mean the natural language sentence. The denotation of the
sentence is [$\Lambda$] and is typically a lambda calculus expression that takes as input
a set of bindings to aspects of the external world. Going from $\Lambda$ to [$\Lambda$] is the
semantic parsing problem. 
In contrast, this work focuses on the mapping between the denotation of an
expression, [$\Lambda$] and the external world $W$ . A denotation consists of a $\lambda$-calculus
expression that takes as input bindings $e_i$ to aspects of $W$ . Our main equation of
interest is the probability that the meaning representation is true when applied
to a particular set of bindings $e$ to the world representation $W$ :
\[p( [\Lambda] (e) = T |W )\]

--- INSERT THE LAMBDA CALCULUS TABLE HERE FROM NOAH'S TEX ---

Each word meaning is represented as a lambda calculus function and the denotation is produced through the composition rules implied by the parse over the input sentence. A set of definitions for the lexicon in our toy model has been presented in Table 1. We would presently illustrate how they can be composed to produce the denotation given a sentence and its parse tree. The meaning of a sentence then corresponds to whether there exists an alignment
that satisfies the denotation predicate:
\[\exists e( [\Lambda] (e) = T )\]
Probabilistically, this expression corresponds to a marginalization over bindings:
\[p(\exists e( [\Lambda] (e) = T )|W ) = \sum_e p(( [\Lambda] (e) = T )|W )\]

Next we provide derivations for specific phrases in the lexicon. Here $e_i$ and
$e_j$ denote references to specific entities in the external world. The first part of
each derivation corresponds to semantic parsing; the second part corresponds
to grounding.

--- INSERT THE FOUR EXAMPLES FROM NOAH'S TEX ---

\section{Implementation}
\subsection{Sentence Parsing}

The first task is to parse the input sentence to form possible lambda expressions or predicates. Our first attempt was at syntactic tree parsing, which lends itself to simple recursive composition through lambda calculus. This, however, has two issues, illustrated through the following example. Consider the sentence "John pickup the red block":
The parse tree: {\tt ROOT (NP (NP (NNP John)) (NP (NN pickup)) (NP (DT the) (JJ red) (NN block))))}
\begin{verbatim}
   Root
     |
---------------------------------
|            |                 |
John	   pickup       -----------------
                        |       |       |
                       the     red     block
\end{verbatim}

The tree structure supports the recursive compositionality of lambda expressions, so that we can compose {\tt the(red(block))} first according to the composition rules, go one level up, and compose {\tt pickedup(john, the(red(block)))}. The issue, however, is in deciding the order of composition. Using lambda expressions alone, if we compose in a particular order, say right-to-left or vise-versa, we run into the wall. One possibility is to do a random composition at each stage, augmented with typed lambda calculus, where we allow only certain kind of compositions, which has been implemented in {\em pythonParse.py}. While this solves part of the problem, we also notice that predicates like {\tt pick(x,y)}, which take multiple arguments, expect those arguments to have certain properties which are not evident from a syntactic parse alone. It is expected that $x$ be the subject and $y$ be the patient, and the log-linear model is built upon these assumptions; unfortunately, that information is not available from a syntactic parse alone. While for active voice sentences, a heuristic based on relative position of the arguments in a sentence can be followed, it can not be generalized. Secondly, even if the above can be handled, we have issues with the composed lambda expressions. The lambda expressions, as they are defined, are intended for a deterministic prediction, while our prime objective is to output a distribution over possible worlds. One possibility is to change the conjunctions in the lambda expressions to products. However, even if it works in most of the cases, in certain examples, this leads to illegal expressions (for example, conjunction of lambdas becomes product of lambdas). To get over these issues, we instead look towards dependency parsing and predicate lists.

\subsection{Sentence to Predicates}
To utilize a log-linear scenario, all we need is to convert a sentence into a list of predicates, which can then be combined. For example, `The red block is on the table' is reduced to {\tt \{red(x), block(x), the(x, 'red block'), table(y), the(y, 'table'), on(x,y)\}}. At present, we are not considering time as a variable, so `is' has no role to play, except to determine which object ($x$ or $y$) goes to which predicate. (This also takes care of an issue with the original lambda expressions. The way `is' has been defined, the lambda expression only works for sentences like `A is B' and fails to produce a valid expression for sentences like `the ball is on the table'.) The pipeline is as follows:
\begin{itemize}
\item Dependency parse the sentence using Stanford parser, which is integrated. Ex: {\tt every block is red}
\begin{verbatim}
det(block-2, every-1)
nsubj(red-4, block-2)
cop(red-4, is-3)
root(ROOT-0, red-4)
\end{verbatim}
\item Use the dependencies like {\em det}, {\em nsubj} to decide on the subject and patient and the bindings. Produce a list of predicates.
Ex: the above sentence should produce predicates {\tt red(x)}, {\tt block(x)} and {\tt every(x, 'block')}. The program output:

{\tt [{'subj': u'2', 'name': u' every', 'func': u'block'}, {'subj': u'2', 'name': u' block'}, {}, {'subj': u'2', 'name': u'red'}]}

\item Accept the world model. In our case, world is a dictionary of objects, with attributes. Use this world model to do log linear inference. 
Example world model: 
\begin{verbatim}
world = 
{
'o1' : {'red':1, 'lower':(3,3,5), 'upper':(5,5,7), 'block':1},
'o2' : {'red':1, 'lower':(0,0,0), 'upper':(5,5,5), 'table':1},
'o3' : {'red':0, 'lower':(0,0,5), 'upper':(2,2,7), 'block':1} 
}
\end{verbatim}
Here, $o1$ etc. are objects, denoted by their bounding boxes (lower and upper are diagonally opposite corners that define this bounding box). The log-linear model has a list of attributes and weights, which define a distribution. An example log-linear model is: 
\begin{verbatim}
block(x) = {
attributes(x) = ['block', 'position', 'color', 'dimension', ...](x) 
              = [x_1, x_2, x_3, ...]
weights = [w_1, w_2, w_3, ...]
return 1/Z * exp(dot_product(attributes(x), weights))
}
\end{verbatim}
The output is the distribution over possible sets of objects. For example, if the world model consists of two blocks, $b_1$ and $b_2$, and a table $t_0$ (objects $o1$, $o3$ and $o2$ respectively in the example world model), then `the block is on the table' is a distribution over the 9 possible assignments to the tuple ('block', 'table') (e.g. ($b_1, b_1$), ($b_1, t_0$) etc. are possible assignments, the later ofcourse having a higher probability). For example, the output inference for `red block on the table' is:
\begin{verbatim}
o3 o3
0.0
o3 o2
0.146995943066
o3 o1
0.0
o2 o3
0.0
o2 o2
0.0
o2 o1
0.0
o1 o3
0.0
o1 o2
0.399576400894
o1 o1
0.0
\end{verbatim}
Notice that the tuple (o1, o2) has the highest unnormalized probability, as is expected given the world model.
\end{itemize}

\noindent
{\Large{ISSUES: }}
\begin{enumerate}
\item We wanted to implement the dependency parser because we needed to distinguish between agent and patient. Dependency parser, however, also fails in the following example: {\tt John put the red block on the red table.}
\begin{verbatim}
                    	put
                        |
-----------------------------------------
|                 |                      |
John            block                    on
                  |                       |
         ---------------              table
         |            |                  |
       red           the              --------------
                                       |           |
                                      the         red
\end{verbatim}

Here we are not sure what is the second argument of `on' from the parse alone.

One way to get over it is to assume that if `put' and `on' are in preposition relation, then the former's 2nd argument = the later's 1st etc. But this argument falls apart for the example: "John put the red block on the red table behind the blue block": Is it {\tt put(John, redBlock, redTable)} or {\tt put(John, redBlock, blueBlock)}, since both `on' and `behind' have the same relation with `put'. May be we need to extend definition of put to change the third variable from object type to location type, in which case the third argumment is `on the red table behind the blue block' : need to explore this. One possibility is to keep the ambiguous argument arbitrary and do inference over it?

\item What function is the argument of `the' in `John picked the red block on the table'. The present parse takes the function as `block'. Standard previous parsing and composition  thereof assumes it is `red $\land$ block', while from observation it looks like `red $\land$ block $\land$ on Table', which is the output of neither of the parses: need to see how this can be handled. This is especially difficult, since similar sentential structures might lead to different interpretations. For instance, in `John put the block on the table', the argument for the first `the' is `block'; but for `John picked the block on the table', the argument is `block on the table'.

\item The implementation of `every' is not complete. While it works in some cases, `every block is red' outputs 1 and .37 for world models with 2 red blocks and 1 red and 1 non-red block respectively, it is failing for `every block is on the table'. Needs looking-into.
\end{enumerate}

------------------------------------------
\newpage
\section{Learning problem formulation}
$n$ individual experts model can be combined as follows:
\[ p({d}|\theta_1\cdots\theta_n) = \frac{\prod_m p_m(d|\theta_m)}{\sum_c\prod_m p_m(c|\theta_m)}\]
where $d$ is the data-vector, $\theta_m$ are parameters of individual model $m$, and $c$ indexes all possible vectors in the data space. Essentially, we have world-sentence pairs, and we derive $d$ and experts from them for each evidence. This is the traditional formulation. Our formulation deviated in a major aspect from this, to wit, the data vector $d$ and the number of experts $p_m(d|\theta_m)$ vary from observation to observation. This, however, shouldn't be that big a problem since at the end of the day, gradiant calculations are performed to update each individual $\theta_m$ and we can just assume that the experts for other missing $\theta_m$ are missing. To be specific, the contrasted divergence formulation reduces to (http://www.cs.toronto.edu/~hinton/absps/tr00-004.pdf): 
\[\Delta \theta_m \propto \frac{\partial \log p_m(d|\theta_m)}{\partial \theta_m} - \frac{\partial \log p_m(\hat{d}|\theta_m)}{\partial \theta_m}\]
So multiple or missing experts can be handled individually for each $\theta_m$. For example, for expressions containing multiple instances of `on()', we would have:
\[\Delta \theta_m \propto \sum_i \frac{\partial \log p_m^i(d|\theta_m)}{\partial \theta_m} - \sum_i \frac{\partial \log p_m^i(\hat{d}|\theta_m)}{\partial \theta_m}\]
though I am not sure how it's going to affect computation or MCMC processes. This problem is especially evident (provided I have understood how the learning algorithm is performing in the paper and am not making a mistake) if we accept the following logic:
`One way to initialize a PoE is to train each expert separately, forcing the experts to differ by giving them diffeerent or differently weighted training cases or by training them on different subsets of the data dimensions, or by using different model classes for the different experts. Once
each expert has been initialized separately, the individual probability distributions need to be
raised to a fractional power to create the initial PoE. Separate initialization of the experts seems like a sensible idea, but simulations indicate that
the PoE is far more likely to become trapped in poor local optima if the experts are allowed to
specialize separately. Better solutions are obtained by simply initializing the experts randomly with very vague distributions and using the learning rule equation.

The Boltzmann machine learning algorithm (Hinton and Sejnowski, 1986) is theoretically elegant and easy to implement in hardware, but it is very slow in networks with interconnected hidden
units because of the variance problems described in section 2. Smolensky (1986) introduced a
restricted type of Boltzmann machine with one visible layer, one hidden layer, and no intralayer connections. Freund and Haussler (1992) realised that in this restricted Boltzmann machine
(RBM), the probability of generating a visible vector is proportional to the product of the probabilities that the visible vector would be generated by each of the hidden units acting alone. An RBM is therefore a PoE with one expert per hidden unit.'

So, the issue is this: if we look at the equation, we realize that we can individually train each expert; however, that might lead to local minimas and lead to inefficient learning. On the other hand, the traditional RBM is not suitable since it's essentially a neural net with specified input and output nodes, the input nodes being the fixed number of experts and the output of a fixed dimension. For example in an image task, the inputs would be a fixed number of pixel values, while in an NLP task, it would be the words from a fixed dictionary. Our problem however involves taking into account multiple observations: our data vector is not of a fixed dimension, depending on different worlds, we have different observations. Our input is also not of a fixed dimension: we could potentially have multiple `on's or other such experts, which do not have a one-to-one mapping to an input node in the neural net. A first stab would be just to combine all evidence of one {\em type} of experts into a single expert, as in the above equation, though I am not sure how that would pan out. 

\subsection{Possible ways these can be handled}
\noindent
{\bf Uncertainty of reference} 

Given a sentence and a world, we are not sure of the referents that go into each expert. Consider a world with three objects, o1, o2 and o3. A product of experts `block(x) on(x,y) table(y)' is ambiguous in the sense that we are not sure which of the o's the x and y variables map to - so we can not determine on(x,y) given a model for on() since we do not know which value to assign to features, i.e. whether to consider values for say o1-o2 interactions or o2-o3 etc. There are multiple scenarios:
\begin{itemize}
\item The referents are known. In that case, there is a perfect 1-1 mapping between the feature values and the model and we are done.
\item The referents are not known. One possibility is to consider every possible mapping an evidence. So, in the running example, we would essentially have 6 possible mappings from o's to x and y. We consider each of them an evidence and update the weight vectors, hoping that there would be enough distinguishing examples to bring the weights down to appropriate values and cancel the noise introduced by assuming all possibilities. The problem with this method is we might have exponential blow-up with complex sentences and worlds.
\item Take a weighted observation based on already learned weights. At stage $t$ of learning, we would have some values for weights of the different experts. It's logical to assume that when new information surfaces, learning is biased towards the belief we already have. So, our evidence would essentially be a linear weighted combination of 6 `block(x) on(x,y) table(y)'s, each pertaining to one assignment, and weighted by the value of that assignment $(1/Z) \sum_\theta p(\theta )block(x_\theta)on(x_\theta,y_\theta) table(y_\theta)$, where Z is the normalizing constant. The good thing is, this method would be robust to noise as once it gets into a favorable minima, it's difficult to drastically change it. The short-coming is of course if the initial evidences are wrong, then it goes into a erroneous state from which it's equally difficult to come out. However, in general, intuitively, it should be faster than the previous method.
\item Hierarchically learn. Assume we have beliefs on `block', `table' etc., experts with single objects. When we decide on `on(x,y)', we can assign weights to particular assignments, and follow the previous approach. For example, in the present case, the probability that x = o1 and y = o2 in on(x,y) would be: block(o1)*table(o2)/($\sum_{o1, o2, o3} block()table()$). Of course, when we learn about blocks further, we can modify their belief from our beliefs on `on' etc.\ each one modifying the other through a recursive procedure. The advantage of this method is that learning experts based on single objects is easier, because the possibilities are linear instead of exponential as in the case of experts with multiple object predicates. So, the learning is expected to be faster in this case. Contrast this to the first method of enumerating all possibilities with a scenario where we have ten-odd objects and 10-odd experts. Also, it kind of reinforces our prior beliefs, so when new evidence/lexicons are encountered, they are easily assigned a strong prior. For example, once we have a strong prior on `on(x,y)', if we come across previously un-encountered lexicon `ball' in an evidence `ball(x) on(x,y)', we can assign a strong belief to `ball()' based on it's possible similarity with `block()', instead of `table()' or `behind()'.
\end{itemize}
I am not sure how much of the above description actually makes sense and how much is just superfluous. From a birds eye view, it seems all the methods are the same. They are all dependent in contrastive learning: given enough distinguishing evidences, they all will be able to generate a proper model. The only gain I see is in computation, as I have described here. But there is possible no way of knowing unless all these are implemented. 

\noindent
{\bf Multiple experts of the same type}

My issues with the RBM  and sampling still remain. How do we take care of multiple experts? The solution alluded to before just takes the average of all the evidences and crams them up in one specific {\em type} of expert-node, instead of the traditional formulation where each expert is a separate node. I am not sure if this should work: but it feels like it should. Or, we can follow the exact same approaches as in the uncertain reference cases to generate a product of unique experts for each type. For example, we can treat `on(x,y) on(u,v)' as `on(a,b)' which is a weighted average of the two, based on already derived weights. I do not have mathematical proof, that would require me to go thorough the whole theory of RBMs to see if all the steps conform to this: but I suggest I just go ahead and implement it and see if it actually works on the data we produce.

\section{Learning Problem Formulation}
We are provided with (world, sentence) pairs and we wish to maximize $p(w|s)$ by choosing suitable parameters $\Theta = \{\theta_i\}_i$ for our log-linear models, where $w$ is the world and $s$ is the sentence. We further denote a particular reference map as $r$. 
\begin{align*}
p(w|s:\Theta) &= \frac{p(w,s:\Theta)}{\sum_w p(w,s:\Theta)}\\
&= \frac{\sum_r p(w,s,r:\Theta)}{\sum_w p(w,s:\Theta)}\\
\Rightarrow\log p(w|s:\Theta) &= \log \sum_r p(w,s,r:\Theta) - \log \sum_w p(w,s:\Theta)\\
\Rightarrow \frac{\partial \log p(w|s:\Theta)}{\partial \theta_i} &= \frac{\partial \log \sum_r p(w,s,r:\Theta)}{\partial \theta_i} - \frac{\partial \log \sum_w p(w,s:\Theta)}{\partial\theta_i}\\
&= \frac{\partial \log \sum_r p(w,s,r:\Theta)}{\partial \theta_i} - \frac{\partial \log \sum_{w,r} p(w,s,r:\Theta)}{\partial\theta_i}\\
&= \frac{1}{\sum_r p(w,s,r:\Theta)} \sum_r \frac{\partial p(w,s,r:\Theta)}{\partial \theta_i}\\
&\ \ \ \  - \frac{1}{\sum_{w,r}p(w,s,r:\Theta)} \sum_{w,r} \frac{\partial p(w,s,r:\Theta)}{\partial\theta_i}
\end{align*}
Now, $p(w,s,r:\Theta)$ is a product of log-linear factors:
\begin{align*}
p(w,s,r:\Theta) &= \prod_i \exp(\theta_i\phi_i(w,s,r))\\
\Rightarrow \frac{\partial p(w,s,r:\Theta)}{\partial \theta_i} &= \phi_i(w,s,r)\prod_i\exp(\theta_i\phi_i(w,s,r))\\
&= \phi_i(w,s,r) p(w,s,r:\Theta)\\
\Rightarrow \frac{\partial \log p(w|s:\Theta)}{\partial \theta_i} &= \frac{1}{\sum_r p(w,s,r:\Theta)} \sum_r \phi_i(w,s,r) p(w,s,r:\Theta)\\
&\ \ \ \  - \frac{1}{\sum_{w,r}p(w,s,r:\Theta)} \sum_{w,r} \phi_i(w,s,r) p(w,s,r:\Theta)\\
&= \sum_r \phi_i(w,s,r)\frac{p(w,s,r:\Theta)}{\sum_r p(w,s,r:\Theta)}   \\
&\ \ \ \  - \sum_{w,r}\phi_i(w,s,r)\frac{ p(w,s,r:\Theta)}{\sum_{w,r}p(w,s,r:\Theta)}  \\
&= \mathbb{E}_{p(r|w,s)} [\phi_i(w,s,r)] - \mathbb{E}_{p(r,w|s)} [\phi_i(w,s,r)]
\end{align*}

\noindent
{\bf Multiple experts of the same type}

If we have multiple experts of the same type, say factor $j$, then $p(w,s,r:\Theta)$ changes to:
\[p(w,s,r: \Theta) = \exp(\theta_j\sum_j\phi_j(w,s,r))\prod_{i\neq j}\exp(\theta_i\phi_i(w,s,r))\]
which reduces to same form as before, if we consider $\sum_j \phi_j(w,s,r)$ as a single factor $\phi_j^{'}  (w,s,r)$. 

\noindent
{\bf Factors introduced by {\tt the}}

The occurrence of the modifies $p(w,s,r:\Theta)$ and introduces factors that are the sum over all possible one-object reference-assignments, and each of them is a product of some log-linear factors and a linear-function of log-linear factors. The general form is as follows:
\begin{align*}
p(w,s,r:\Theta) &= \left(\prod_i \exp(\theta_i\phi_i)\right) \prod\left(\sum_{\hat{r}} \left(\prod\exp(\theta_i\phi_i^{\hat{r}})\prod_{{\hat{r}}'\neq {\hat{r}}} (1-\prod\exp(\theta_i\phi_i^{{\hat{r}}'}))\right)\right)
\end{align*}
This form, unfortunately, doesn't reduce to a suitable expectation (or at least, I couldn't reduce it to a compact form).

\noindent
{\bf Factors introduced by [{\tt every ... is}]}

\begin{align*}
p(w,s,r: \Theta) &= \prod_{\hat{r}} (a_{\hat{r}} + \sum_i \exp(\theta_i\phi_i^{\hat{r}}))\\
\Rightarrow \frac{\partial p(w,s,r:\Theta)}{\partial \theta_j} &= \sum_{{\hat{r}}'} \left(\phi_j^{{\hat{r}}'} \exp(\theta_j\phi_j^{{\hat{r}}'})\left(\prod_{{\hat{r}}\neq {\hat{r}}'}  (a_{{\hat{r}}} + \sum_i \exp(\theta_i\phi_i^{\hat{r}}))\right)\right)
\end{align*}
which is not reducible to a compact form either.

While these factors break our mathematical formula involving expectations, the problem might be addressed as follows:

\noindent
The appearance of {\tt the} etc.\ doesn't have a significant effect on model parameters $\theta$. Consider for example {\tt the block}, which reduces to 
\[p(w,s,r:\Theta) = {block}(x|r)\sum_{\hat{r}} \left(block(x|{\hat{r}})\prod_{{\hat{r}}'\neq {\hat{r}}}(1-block(x|\hat{r}'))\right). \]
Notice that the second term (which is mathematically the probability that the assignment $r$ is unique) in effect just denotes the probability of the given assignment $r$. The parameter to be optimized, $\theta_{block}$ still resides in the first factor. So we might as well use the same formulation of derivative for parameters as in the equation derived before, the difference being that $p(r|w,s)$ is now determined by factors like the second term, i.e. use of {\tt the} essentially modifies the probability assigned to each reference-map (which is what it actually does: if we have only one block and two balls in a scenario, $block(x|r)$ should have a high probability for r=block if the sentence is `the block'). Similar approach can be taken for other tokens like {\tt is} and {\tt every} where they just modify the probabilities in the expectation. Minimal information is lost because all experts still occur as products in the rest of $p(w,s,r)$. 
(Need to confirm if this assumption makes sense.)










\end{document}

