import string
import jpype
import os
import nltk
from nltk.tree import *
import sys
from random import shuffle
import copy
import re

class DependencyParser:

    def __init__(self, sent = ""):
        self.sent = sent
        self.parsedObj = {}
        self.parsedObj['objects'] = []
        self.parsedObj['parse'] = [{} for _ in range(0,len(sent.split()))]
        self.toDo = []

    def nsubj(self,x,y):                                   
        x = x.split('-')                                   
        y = y.split('-')                                       
        if y[1] not in self.parsedObj['objects']:              
            self.parsedObj['objects'].append(y[1])             
        self.parsedObj['parse'][int(x[1])-1]['name'] = x[0]    
        self.parsedObj['parse'][int(x[1])-1]['subj'] = y[1]    
        self.parsedObj['parse'][int(y[1])-1]['name'] = y[0]    
        self.parsedObj['parse'][int(y[1])-1]['subj'] = y[1]    
                                                               
    def dobj(self,x,y):                                        
        x = x.split('-')                                   
        y = y.split('-')                                       
        if y[1] not in self.parsedObj['objects']:              
            self.parsedObj['objects'].append(y[1])             
        self.parsedObj['parse'][int(x[1])-1]['name'] = x[0]    
        self.parsedObj['parse'][int(x[1])-1]['obj1'] = y[1]    
        self.parsedObj['parse'][int(y[1])-1]['name'] = y[0]    
        self.parsedObj['parse'][int(y[1])-1]['subj'] = y[1]    
                                                               
    def pobj(self,x,y):                                        
        x = x.split('-')                                   
        y = y.split('-')                                       
        if y[1] not in self.parsedObj['objects']:              
            self.parsedObj['objects'].append(y[1])             
        self.parsedObj['parse'][int(x[1])-1]['name'] = x[0]    
        self.parsedObj['parse'][int(x[1])-1]['obj1'] = y[1]    
        self.parsedObj['parse'][int(y[1])-1]['name'] = y[0]    
        self.parsedObj['parse'][int(y[1])-1]['subj'] = y[1]    
                                                               
    def amod(self,x,y):                                        
        x = x.split('-')                                   
        y = y.split('-')                                       
        if x[1] not in self.parsedObj['objects']:              
            self.parsedObj['objects'].append(x[1])             
        self.parsedObj['parse'][int(x[1])-1]['name'] = x[0]    
        self.parsedObj['parse'][int(x[1])-1]['subj'] = x[1]    
        self.parsedObj['parse'][int(y[1])-1]['name'] = y[0]    
        self.parsedObj['parse'][int(y[1])-1]['subj'] = x[1]    
                                                               
    def det(self,x,y):                                         
        x = x.split('-')                                   
        y = y.split('-')                                       
        if x[1] not in self.parsedObj['objects']:              
            self.parsedObj['objects'].append(x[1])             
        self.parsedObj['parse'][int(x[1])-1]['name'] = x[0]    
        self.parsedObj['parse'][int(x[1])-1]['subj'] = x[1]    
        self.parsedObj['parse'][int(y[1])-1]['name'] = y[0]    
        self.parsedObj['parse'][int(y[1])-1]['func'] = x[0]    
        
    def prep(self,x,y):
        self.toDo.append([x,y]) 

    def root(self,x,y):
        pass

    def cop(self,x,y):
        pass
    
    def createParse(self):
        
        """ The following code snippet is the interface to Sanford Parser, from Luke allen. Make sure to change the JAVA_HOME path variables and jar file paths to your system specifications before using."""

        '''
        @author: Luke Allen
        To make this program work, you need to also:
        -Install JPype, for your Python version, from http://jpype.sourceforge.net/
        -Download the Stanford Parser from http://nlp.stanford.edu/software/lex-parser.shtml
        and copy its .jar files to a folder called "../jar/" relative to this .py file, 
        or else change the line labeled "directory that .jar files are in" below.
        '''
        print "Parsing sentence ...\n"
        print self.sent
        os.environ['java_home']='/usr/lib/jvm/java-7-oracle/'
        os.environ['JAVA_HOME']='/usr/lib/jvm/java-7-oracle/'
        jarpath = os.path.join(os.path.abspath('.'), "./StanParser") #directory that .jar files are in, relative to this .py script.
        jpype.startJVM(jpype.getDefaultJVMPath(),"-ea", "-Djava.ext.dirs=%s" % jarpath) #start a JVM
        nlp = jpype.JPackage("edu.stanford.nlp") #this is how you import a package (which contains classes). Now we can use the classes inside it, almost as easily as Java.
#code below is a simplified version of the ParserDemo.java included with the Stanford Parser
        lp = nlp.parser.lexparser.LexicalizedParser.loadModel() #call a static method of a class like this (see the Stanford Parser's Javadoc for a description of its methods and classes) 
        sent = self.sent
#sent = "John put the red block on the red table"
        parse = lp.apply(sent)#now we have a Tree class and can do what we want with it
        #print parse
        parse.pennPrint();
        tparse = nltk.tree.Tree.parse
        tree = tparse(parse.__str__())
        #print tree

        # tree.draw()
        # for subtree in tree:
        #     print subtree
        #print "Grammatical Structure:"
        tlp = nlp.trees.PennTreebankLanguagePack()
        gsf = tlp.grammaticalStructureFactory()
        gs = gsf.newGrammaticalStructure(parse)
        tdl = gs.typedDependencies()#CCprocessed(False)
        #print tdl


        functions = {'nsubj':self.nsubj, 'dobj':self.dobj, 'pobj':self.pobj, 'amod':self.amod, 'det':self.det, 'prep':self.prep, 'root':self.root, 'cop':self.cop}

        for item in tdl:
            print item
            item = re.split('[(,)]', item.__str__())
            functions[item[0]](item[1], item[2])

        #print self.parsedObj    
        for item in self.toDo:                                                                                                  
            #print item                                                                                                     
            x,y = item[0], item[1]                                                                                         
            if x.split('-')[0] == 'is':                                                                                    
                    self.parsedObj['parse'][int(y.split('-')[1])-1]['subj'] = self.parsedObj['parse'][int(x.split('-')[1])-1]['subj']
                    continue                                                                                               
                                                                                                                           
            self.parsedObj['parse'][int(x.split('-')[1])-1]['obj2'] = self.parsedObj['parse'][int(y.split('-')[1])-1]['obj1']        
            self.parsedObj['parse'][int(y.split('-')[1])-1]['subj'] = self.parsedObj['parse'][int(x.split('-')[1])-1]['obj1']        

        #print "The predicates are: "
        #print self.parsedObj['parse']

        jpype.shutdownJVM() #call at the end of the script

    def returnParse(self):
        self.createParse()
        return self.parsedObj
